/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

'use strict';


const Clutter = imports.gi.Clutter;
const Lang = imports.lang;

const St = imports.gi.St;
const GObject = imports.gi.GObject;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;

const Main = imports.ui.main;
const Panel = imports.ui.panel;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const MessageTray = imports.ui.messageTray;
const AggregateMenu = Main.panel.statusArea.aggregateMenu;
const AggregateLayout = Panel.AggregateLayout;
const Shell = imports.gi.Shell;
const Util = imports.misc.util;

//const Util = imports.misc.util;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
//const Utils = Me.imports.utils;

const Format = imports.format;
const Gettext = imports.gettext.domain('arch-update');

const components = Me.imports.components;
const utils = Me.imports.components.utils;
const UpdateManager = utils.UpdateManager;
const exec = utils.execute;
//const MultiSubMenuItem = components.popupMMenu.MultiSubMenuItem;

const HOME_DIR = GLib.get_home_dir();
const USER_CACHE_DIR = GLib.get_user_cache_dir();
const SCRIPT_DIR = Me.dir.get_child('scripts').get_path();

/* Default options */
let CHECK_INTERVAL = 60 * 120;
let NOTIFY = true;
let UPDATE_CMD = '/usr/bin/yay -Su';
let PACMAN_DIR = '/var/lib/pacman/local';
let CHECK_UPD_CMD = SCRIPT_DIR + 'output_update.sh';
let RECHECK_UPDT_CMD = SCRIPT_DIR + 'recheckUpdates.py'

class PopupSubSubMenu extends PopupMenu.PopupSubMenu {
    constructor(sourceActor, sourceArrow) {
        super(sourceActor, sourceArrow);
    }

    open(animate) {
        if (this.isOpen)
            return;

        if (this.isEmpty())
            return;

        this.isOpen = true;
        this.emit('open-state-changed', true);

        this.actor.show();

        let needsScrollbar = this._needsScrollbar();

        // St.ScrollView always requests space horizontally for a possible vertical
        // scrollbar if in AUTOMATIC mode. Doing better would require implementation
        // of width-for-height in St.BoxLayout and St.ScrollView. This looks bad
        // when we *don't* need it, so turn off the scrollbar when that's true.
        // Dynamic changes in whether we need it aren't handled properly.
        this.actor.vscrollbar_policy =
            needsScrollbar ? St.PolicyType.AUTOMATIC : St.PolicyType.NEVER;

        if (needsScrollbar)
            this.actor.add_style_pseudo_class('scrolled');
        else
            this.actor.remove_style_pseudo_class('scrolled');

        // It looks funny if we animate with a scrollbar (at what point is
        // the scrollbar added?) so just skip that case
        if (animate && needsScrollbar)
            animate = false;

        let targetAngle = this.actor.text_direction == Clutter.TextDirection.RTL ? -90 : 90;

        if (animate) {
            let [, naturalHeight] = this.actor.get_preferred_height(-1);
            this.actor.height = 0;
            this.actor.ease({
                height: naturalHeight,
                duration: 250,
                mode: Clutter.AnimationMode.EASE_OUT_EXPO,
                onComplete: () => this.actor.set_height(-1)
            });
            this._arrow.ease({
                rotation_angle_z: targetAngle,
                duration: 250,
                mode: Clutter.AnimationMode.EASE_OUT_EXPO
            });
        } else {
            this._arrow.rotation_angle_z = targetAngle;
        }
    }

    close(animate) {
        if (!this.isOpen)
            return;

        this.isOpen = false;
        this.emit('open-state-changed', false);

        if (this._activeMenuItem)
            this._activeMenuItem.active = false;

        if (animate && this._needsScrollbar())
            animate = false;

        if (animate) {
            this.actor.ease({
                height: 0,
                duration: 250,
                mode: Clutter.AnimationMode.EASE_OUT_EXPO,
                onComplete: () => {
                    this.actor.hide();
                    this.actor.set_height(-1);
                }
            });
            this._arrow.ease({
                rotation_angle_z: 0,
                duration: 250,
                mode: Clutter.AnimationMode.EASE_OUT_EXPO
            });
        } else {
            this._arrow.rotation_angle_z = 0;
            this.actor.hide();
        }
    }

    _onKeyPressEvent(actor, event) {
        // Move focus back to parent menu if the user types Left.

        if (this.isOpen && event.get_key_symbol() == Clutter.KEY_Left) {
            this.close(BoxPointer.PopupAnimation.FULL);
            this.sourceActor._delegate.active = true;
            return Clutter.EVENT_STOP;
        }

        return Clutter.EVENT_PROPAGATE;
    }
}

var PackageMenuItem = GObject.registerClass(
    class PackageMenuItem extends PopupMenu.PopupSubMenuMenuItem {
        _init(pkg) {
            super._init(pkg.pkg_name, true);
            this.package = pkg;

            this._updateInfomenuLabel = new St.Label({
                y_align: Clutter.ActorAlign.CENTER,
                x_align: Clutter.ActorAlign.CENTER
            });

            this.add_style_class_name('package-menu-item');
            if (this.package.repo === undefined) {
                this.package.repo = 'Sconosciuta';
            }
            this.menu = new PopupSubSubMenu(this, this._triangle);
            this.menu.box.add(this._updateInfomenuLabel);
            let labellist = [
                _('Pacchetto: ' + this.package.pkg_name),
                _('Versione: ' + this.package.pkg_version),
                _('Nuova versione: ' + this.package.pkg_newVersion),
                _('Fonte: ' + this.package.repo)
            ]
            this._updateInfomenuLabel.set_text(labellist.join('\n'));
        }
    });

var UpdateMenuSection = GObject.registerClass(
    class UpdateMenuSection extends PopupMenu.PopupSubMenuMenuItem {
        _init() {
            super._init('', false);

            this._updateManager = new UpdateManager();
            this._menuVoices = new Array();
            this._updatesList = new Array();
            this._updateMenu = new Array();
            this.updateNum = null;
            this._notifySrc = null;


            this.add_style_class_name('update-menu-section');

            this.menu = new PopupSubSubMenu(this, this._triangle);
        }

        checkUpdates() {
            this._updateManager.check_updates();
            this._updatesList = this._updateManager.pkgList;
            this.updateNum = this._updateManager.updateNum;

            this._updateMenuLabel();
            this._createUpdateList();

            if (this.isUpdatesAvailable()) {
                this._showNotification(this.updateNum.toString() + 'aggiornamenti disponibili', this._updateManager.pkgNameList.join(', '));
            }
        }

        isUpdatesAvailable() {
            return (this.updateNum > 0);
        }

        _updateMenuLabel() {
            if (this.isUpdatesAvailable()) {
                this.actor.reactive = true;
                this._triangle.visible = true;
                this.actor.visible = true;
                this.label.set_text(_(this.updateNum + ' aggiornamenti disponibili'));
            } else {
                this.actor.reactive = false;
                this._triangle.visible = false;
                this.label.set_text(_('Il sistema è aggiornato'));
            }
        }

        _createUpdateList() {
            var menuVoices = new Array();
            for (var i = 0; i < this.updateNum; i++) {
                var pkg_voiceMenu = new PackageMenuItem(this._updatesList[i]);
                menuVoices.push(pkg_voiceMenu);
            }

            this._menuVoices = menuVoices;

            this._updateMenuItems();
        }

        _updateMenuItems() {
            if (this._updateMenu.length > 0 && this._updateMenu !== this._menuVoices) {
                this.menu.removeAll();
            }
            this._updateMenu = this._menuVoices;

            var l = this._updateMenu.length;
            if (l == 0) return;
            for (var i = 0; i < l; i++) {
                this.menu.addMenuItem(this._updateMenu[i]);
            }
            this.menu.addAction(_('Aggiorna ora'), () => this.updateNow());
        }

        updateNow() {
            this._showNotification("prova", "porcoddiooooooooooo");
        }

        _closeUpdateMenuItems() {
            var l = this._updateMenu.length;

            for (var i = 0; i < l; i++) {
                this._updateMenu[i].menu.close();
            }
        }

        _showNotification(title, msg_text) {
            if (this._notifySrc === null) {
                log("Creating notification sources");
                this._notifySrc = new MessageTray.SystemNotificationSource();
                this._notifySrc.createIcon = function () {
                    let icon = new St.Icon({
                        gicon: new Gio.ThemedIcon({ name: 'software-update-available-symbolic' })
                    });

                    return icon.gicon;
                };
                this._notifySrc.connect('destroy', () => { this._notifySrc = null });

                Main.messageTray.add(this._notifySrc);
            }
            let notification = null;

            if (this._notifySrc.notifications.length == 0) {
                notification = new MessageTray.Notification(this._notifySrc, title, msg_text);
                notification.addAction(_('Aggiorna ora'), () => this.updateNow());
            } else {
                notification = this._notifySrc.notifications[0];
                notification.update(title, msg_text, { clear: true });
            }

            notification.setTransient(false);
            this._notifySrc.notify(notification);
        }

        destroy() {
            if (this._notifySrc) {
                this._notifySrc.destroy();
                this._notifySrc = null;
            }
            this._updateManager.destroy();
            this._updateManager = null;
            this._menuVoices.destroy();
            this._menuVoices = null;
            this._updatesList.destroy();
            this._updatesList = null;
            this._updateMenu.destroy();
            this._updateMenu = null;

            super.destroy();
        }
    }
);

//const _ = Gettext.gettext;
/**
 *  @class ServiceIndicator
 */
class ServiceIndicator extends PanelMenu.SystemIndicator {

    constructor() {
        super();

        this._menuVoices = new Array();
        this._updateNum = null;
        this._pkgManager = function () { Util.spawn(['gnome-software']) };
        this._updatesList = new Array();
        this._processUpdater = null;
        this._timeout = null;
        this._swCenter = null;
        this._updatePending = false;
        this._folderChangeTimeoutId = null;
        this.monitor = null;
        this._timeoutId = null;
        let icon = new St.Icon({
            gicon: new Gio.ThemedIcon({ name: 'software-update-available-symbolic' }),
            style_class: 'system-status-icon'
        });

        this._buttonText = new St.Label({
            y_align: Clutter.ActorAlign.CENTER
        });
        this._buttonText.visible = true;
        this._buttonText.reactive = true;

        this._icon = icon.gicon;

        //Create service indicator
        this._indicator = this._addIndicator();
        this._indicator.gicon = this._icon;
        this._indicator.visible = false;
        this.indicators.add_child(this._buttonText);

        AggregateMenu._indicators.insert_child_at_index(this.indicators, 0);

        // ServiceMenu
        this._item = new PopupMenu.PopupSubMenuMenuItem(_('Aggiornamenti'), true);
        this._item.icon.gicon = this._indicator.gicon;
        this._item.label.clutter_text.x_expand = true;
        this._item.label.y_expand = true;

        // ServiceMenu -> UpdateSubMenu
        this.updateSubMenu = new UpdateMenuSection('')

        //     let container = new SubSubSection(this.updateSubMenu);

        // Check update at start
        this.initialize();

        // UpdateMenu -> UpdateSubMenu
        this._item.menu.addMenuItem(this.updateSubMenu);

        // Menu Actions
        //this._item.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
        this._item.menu.addAction(_("Controlla aggiornamenti"), () => this.checkUpdates());
        this._item.menu.addAction(_("Apri gestore pacchetti"), this._pkgManager);
        this._item.menu.addAction(_('Impostazioni aggiornamenti'), this._openExtSetting);

        this.menu.addMenuItem(this._item);

        AggregateMenu.menu.addMenuItem(this.menu, 4);
    }

    _startFolderMonitor() {
        this.pacman_dir = Gio.file_new_for_path('/var/lib/pacman/local/')
        this.monitor = this.pacman_dir.monitor_directory(0, null);
        this.monitor.connect('changed', () =>  this._onFolderChanged());
    }

    _onFolderChanged() {
        let that = this;
        if (this._folderChangeTimeoutId) GLib.source_remove(this._folderChangeTimeoutId);
        this._folderChangeTimeoutId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 10, function () {
            that.checkUpdates();
            that._folderChangeTimeoutId = null;
            return false;
        });
    }

    recheckUpdates() {
        this.checkUpdates();
    }
    _apply_settings() {

    }

    _start_check_tas() {
        if (this._timeoutId) GLib.source_remove(this._timeoutId);
        this._timeoutId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, CHECK_INTERVAL, function () {
            that.checkUpdates();
            return true;
        });
    }
    initialize() {
        this.checkUpdates();
        this._startFolderMonitor();
    }

    checkUpdates() {

        this.updateSubMenu.checkUpdates();

        this._updatePending = this.updateSubMenu.isUpdatesAvailable();
        this._updateNum = this.updateSubMenu.updateNum;

        this._updateIndicator(this._updatePending);
    }
    _updateIndicator(updateIsAvailable) {
        if (updateIsAvailable) {
            this._indicator.visible = true;
            this._buttonText.visible = true;
            this._buttonText.set_text(this._updateNum.toString());
        } else {
            this._indicator.visible = false;
            this._buttonText.visible = false;
        }
    }

    destroy() {
        if (this.monitor) {
            this.monitor.cancel();
            this.monitor = null;
        }

        if (this._folderChangeTimeoutId) {
            GLib.source_remove(this._folderChangeTimeoutId);
            this._folderChangeTimeoutId = null;
        }

        if (this._timeoutId)
            delete AggregateMenu._updater;
        this.indicators.destroy();
        this.updateSubMenu.destroy();
        this._item.destroy();
        this.menu.destroy();
    }
}
var serviceIndicator = null;
function init() {

}

function enable() {
    serviceIndicator = new ServiceIndicator();
}

function disable() {
    serviceIndicator.destroy();
    serviceIndicator = null;
}