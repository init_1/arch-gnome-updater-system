imports.gi.versions.Gtk = '3.0'

const { GObject, Gtk, Gio, GLib } = imports.gi;

let PASSWORD = null;

Gtk.init(null);

var UpdaterPasswordDialog = GObject.registerClass(
    class UpdaterPasswordDialog extends Gtk.Window {
        _init() {
            super._init({
                title: "Inserire password",
                icon_name: 'dialog-password',
                resizable: false,
                startup_id: 'Gnome Updater System',
                urgency_hint: true,
                role: 'Gnome Update System'
            });
            this.set_keep_above(true);

            this.password = null;
            this.savePassword = false;

            this.box = new Gtk.Box({
                border_width: 10,
                orientation: Gtk.Orientation.VERTICAL,
            });

            let passbox = new Gtk.Box({
                orientation: Gtk.Orientation.HORIZONTAL,
            });
            let checkbox = new Gtk.Box();

            this._passcheckBox = new Gtk.CheckButton({ label: 'Ricorda password' });

            this._entry = new Gtk.Entry({
                visibility: false
            });
            //this._entry. = true;

            this._button = new Gtk.Button({
                label: 'OK',
                margin_right: 0,
                margin_left: 5,
                // margin_top: 10
            });
            this._button.connect("clicked", () => this._savePasswordAndExit());
            this._entry.connect('activate', () => this._savePasswordAndExit());
            this._passcheckBox.connect('toggled', () => { this.savePassword = !this.savePassword })
            passbox.add(this._entry);
            passbox.add(this._button);

            checkbox.add(this._passcheckBox);


            this.box.add(passbox);
            this.box.add(checkbox);

            this.add(this.box);

            // this.set_default_size(300, -1)
            this.set_position(Gtk.WindowPosition.CENTER);

            this._entry.set_input_purpose(Gtk.InputPurpose.PASSWORD);
            this._entry.caps_lock_warning = true;
            this._entry.invisible_char_set = true;
        }

        _savePasswordAndExit() {
            this.password = this._entry.get_buffer().text;
            this.close();
        }

        show_dialog() {
            this.connect("destroy", () => Gtk.main_quit());
            this.show_all();
            Gtk.main();
        }
    }
);

/*
let win = new UpdaterPasswordDialog();
win.show_dialog();

if (win.savePassword) log(win.password);

print(win.password);
*/