const { GLib, Gio } = imports.gi;

var execute = function execute(cmd) {
    var arr = cmd.split(' ');

    var [res, out] = GLib.spawn_sync(null, arr, null, GLib.SpawnFlags.SEARCH_PATH, null);

    if (out == null) {
        return _("Il sistema è aggiornato");
    }
    else {
        return out.toString();
    }
}

var Package = function Package(name, version, n_version, repo) {
    this.pkg_name = name;
    this.pkg_version = version;
    this.pkg_newVersion = n_version;
    this.repo = repo;
}

var UpdateManager = class UpdateManager {
    constructor() {
        this.pkgList = new Array();
        this.pkgNameList = new Array();
        this.updateNum = null;
    }
    
    _ask_password() {
        
    }
    
    check_updates() {
        var pkgList = new Array;
        var update_str = execute('/usr/bin/yay -Qu');
        var name_only_str = execute('/usr/bin/yay -Quq');
        
        this.pkgNameList = name_only_str.split('\n');

        var arr = update_str.split('\n');
        var updateNum = arr.length-1;

        var i = 0;
        while (i < updateNum) {
            var wrd = arr[i].split(' ');

            var name = wrd[0];
            var version = wrd[1];
            var n_version = wrd[3];

            var pkg = new Package(name, version, n_version);
            pkgList.push(pkg);
            i++;
        }
        this.updateNum = updateNum;
        this.pkgList = pkgList;
    }

    destroy() {
        this.pkgList.destroy();
    }
}